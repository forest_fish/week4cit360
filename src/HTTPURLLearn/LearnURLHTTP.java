package HTTPURLLearn;

import java.net.*;
import java.io.*;
import java.util.*;


public class LearnURLHTTP {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        URL url = GetURL(in);
        String page = (GetHTTPContent(url));
        SearchPage(in, page);
    }


    public static URL GetURL(Scanner in) {
        URL url = null;
        String myUrl;
        System.out.println("Please enter the url of a website you would like to search. (ex: https://www.google.com/)");

        try {
            myUrl = in.nextLine();
            url = new URL(myUrl);
        } catch (MalformedURLException e) {
            System.out.println("The url you entered is not valid. Please enter a valid url. (ex: https://www.google.com/)");
            url = GetURL(in);
        }
        return url;
    }


    public static String GetHTTPContent(URL url) {
        String page = "No info retrieved.";
        try {
            System.out.println("---Loading---");
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder builder = new StringBuilder();

            while ((page = reader.readLine()) != null) {
                builder.append(page + "\n");
            }
            page = builder.toString();

        } catch (IOException e) {
            System.out.println("There was an issue reading the page you entered.");
        }
        return page;
    }


    public static void SearchPage(Scanner in, String page) {
        System.out.println("What word or phrase would you like to search for?");
        String key = in.nextLine();
        List occurrence = new ArrayList();

        if (page.indexOf(key) == -1) {
            System.out.println(key + " was not found on the page.");
        } else {
            int i = 0;
            boolean notDone = true;
            while (notDone) {
                if (page.indexOf(key, i) != -1) {
                    occurrence.add(page.indexOf(key, i));
                    i = page.indexOf(key, i) + 1;
                } else {
                    notDone = false;
                }
            }
        }

        System.out.println(key + " was found " + occurrence.size() + " times.");
        System.out.println("Would you like to search for something else? (y or n)");
        String answerString = "";

        while (!(answerString.equalsIgnoreCase("y")) || !(answerString.equalsIgnoreCase("n"))) {
            answerString = in.nextLine();
            if (answerString.equalsIgnoreCase("y")) {
                SearchPage(in, page);
            } else if (answerString.equalsIgnoreCase("n")) {
                System.out.println("Thank you. Have a nice day!");
            } else {
                System.out.println("You must enter a y or n.");
            }
        }
    }
}
